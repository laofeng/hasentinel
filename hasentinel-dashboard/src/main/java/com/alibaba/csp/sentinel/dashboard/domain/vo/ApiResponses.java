package com.alibaba.csp.sentinel.dashboard.domain.vo;

import java.io.Serializable;
import java.util.Objects;

/**
 * 系统封装的统一web响应结构体
 * 
 * @param <T>
 */
public class ApiResponses<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 表示接口调用成功
	 */
	public static final int SUCCESS = 1;
	/**
	 * 表示接口调用失败
	 */
	public static final int FAIL = -1;

	public static final String SUCC_MSG = "成功";
	public static final String FAIL_MSG = "失败";

	private String msg = SUCC_MSG;
	private int result = SUCCESS;// 状态码 1:成功, -1:失败, ......

	/**
	 * 结果集返回
	 */
	private T data;

	/**
	 * 返回成功
	 */
	public static ApiResponses<Void> success() {
		return ApiResponses.<Void>builder().result(SUCCESS).msg(SUCC_MSG).build();
	}

	/**
	 * 成功返回
	 *
	 * @param object
	 */
	public static <T> ApiResponses<T> success(T object) {
		return ApiResponses.<T>builder().result(SUCCESS).msg(SUCC_MSG).data(object).build();
	}

	/**
	 * 返回失败
	 */
	public static ApiResponses<Void> failure() {
		return ApiResponses.<Void>builder().result(FAIL).msg(FAIL_MSG).build();
	}

	/**
	 * 返回失败
	 */
	public static ApiResponses<Void> failure(String msg) {
		return ApiResponses.<Void>builder().result(FAIL).msg(msg).build();
	}

	/**
	 * 返回失败
	 */
	public static ApiResponses<Void> failure(int result, String msg) {
		return ApiResponses.<Void>builder().result(result).msg(msg).build();
	}

	public static <T> ApiResponses.ApiResponsesBuilder<T> builder() {
		return new ApiResponses.ApiResponsesBuilder<T>();
	}

	public ApiResponses(String msg, int result, T data) {
		this.msg = msg;
		this.result = result;
		this.data = data;
	}

	public ApiResponses() {
	}

	public String getMsg() {
		return msg;
	}

	public int getResult() {
		return result;
	}

	public T getData() {
		return data;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ApiResponses<?> that = (ApiResponses<?>) o;
		return result == that.result && msg.equals(that.msg) && data.equals(that.data);
	}

	@Override
	public int hashCode() {
		return Objects.hash(msg, result, data);
	}

	public static class ApiResponsesBuilder<T> {
		private String msg;
		private int result;
		private T data;

		ApiResponsesBuilder() {
		}

		public ApiResponses.ApiResponsesBuilder<T> msg(String msg) {
			this.msg = msg;
			return this;
		}

		public ApiResponses.ApiResponsesBuilder<T> result(int result) {
			this.result = result;
			return this;
		}

		public ApiResponses.ApiResponsesBuilder<T> data(T data) {
			this.data = data;
			return this;
		}

		public ApiResponses<T> build() {
			return new ApiResponses<T>(this.msg, this.result, this.data);
		}

		@Override
		public String toString() {
			return "ApiResponses.ApiResponsesBuilder(msg=" + this.msg + ", result=" + this.result + ", data="
					+ this.data + ")";
		}
	}
}
